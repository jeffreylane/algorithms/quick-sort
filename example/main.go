package main

import (
	"fmt"

	quicksort "gitlab.com/jeffreylane/algorithms/quick-sort"
	"gitlab.com/jeffreylane/shuffle"
)

const (
	before    string = "array before being shuffled:\n%v\n\n"
	after     string = "array after being shuffled:\n%v\n\n"
	quickSort string = "array after %v quicksort:\n%v\n\n"
)

func main() {
	// Create a large array, shuffle it, then sort it using quick sort
	arrSize := 100
	var arr []int
	for i := 0; i < arrSize; i++ {
		arr = append(arr, i+1)
	}
	fmt.Printf(before, arr)
	shuffled := shuffle.Shuffle(arr)
	fmt.Printf(after, shuffled)

	// Declare a func to define the left/right arrays based on pivotValue
	lessthan := func(a, b int) bool {
		return a < b
	}
	greaterthan := func(a, b int) bool {
		return a > b
	}
	min, max := 0, len(shuffled)-1
	sorted := quicksort.QuickSort(shuffled, min, max, lessthan)
	fmt.Printf(quickSort, "asc", sorted)
	desc := quicksort.QuickSort(shuffled, min, max, greaterthan)
	fmt.Printf(quickSort, "desc", desc)
}
