package quicksort

// sorts slice of type A using quick-sort algorithm. caller provides comparer func
// which determines the boolean of the boolean operator of index (a) curIdx,
// and (p), the index of pivot element, and whether to swap highest to lowest value
// (desc) or the inverse (asc).
// For example:
/*
	lessthan := func(a, p int) bool { // asc
		return a < p
	}
	greaterthan := func(a, p int) bool { // desc
		return a > p
	}
*/
func QuickSort[A ~[]E, E any](a A, min, max int, comparer func(E, E) bool) A {
	if min < max {
		a, splitIdx := divide(a, min, max, comparer)
		a = QuickSort(a, min, splitIdx-1, comparer)
		return QuickSort(a, splitIdx+1, max, comparer)
	}
	return a
}

// returns the divided array with it's split index (the pivot value's index)
func divide[A ~[]E, E any](a A, min, max int, comparer func(E, E) bool) (A, int) {
	pivot := a[max]
	i := min
	for j := min; j < max; j++ {
		if comparer(a[j], pivot) {
			a = swap(a, i, j)
			i++
		}
	}
	a = swap(a, i, max)
	return a, i
}
func swap[A ~[]E, E any](a A, min, max int) A {
	a[max], a[min] = a[min], a[max]
	return a
}
